extends RigidBody2D

export var start_speed = 500
export var min_speed = 200
export var max_speed = 1000
export var max_rotation_speed = 50

signal destroyed(ball)

# Called when the node enters the scene tree for the first time.
func _ready():
	var initial_velocity = Vector2()
	initial_velocity.x = randf() - 0.5
	initial_velocity.y = randf() - 0.5
	initial_velocity = initial_velocity.normalized() * start_speed
	linear_velocity = initial_velocity

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	print("ball")

func _on_screen_exited():
	emit_signal("destroyed", self)

func _integrate_forces(_state):
	# Clamp ball speed within certain limits
	var speed = linear_velocity.length()
	speed = clamp(speed, min_speed, max_speed)
	linear_velocity = linear_velocity.normalized() * speed
	
	# Clamp ball rotation within certain limits
	angular_velocity = clamp(angular_velocity, -max_rotation_speed, max_rotation_speed)
