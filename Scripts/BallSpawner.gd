extends Node2D

signal please_spawn_ball(pos)

var spawning

# Called when the node enters the scene tree for the first time.
func _ready():
	cleanup()

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func start_ball_spawn():
	# Don't try to spawn two bals at once
	if spawning:
		return
	$SpawnTimer.start()
	$CooldownTimer.start()
	$Particles2D.emitting = true
	$AudioStreamPlayer.play()
	spawning = true

func emit_please_spawn_ball():
	emit_signal("please_spawn_ball", position)
	$Particles2D.emitting = false

func cooldown():
	spawning = false

func cleanup():
	spawning = false
	$SpawnTimer.stop()
	$CooldownTimer.stop()
	$Particles2D.emitting = false
	$AudioStreamPlayer.stop()
