extends Area2D

export var yeet_strength = 5000

# Called when the node enters the scene tree for the first time.
func _ready():
	$AnimatedSprite.animation = "default"

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _hit_by_ball(ball):
	# Fling ball
	var yeet_direction = ball.position - position
	var yeet = yeet_direction.normalized() * yeet_strength
	ball.apply_central_impulse(yeet)
	# Play boop animation
	$AnimatedSprite.stop()
	$AnimatedSprite.animation = "boop"
	$AnimatedSprite.frame = 0
	$AnimatedSprite.play()
	# Play boing sound
	$AudioStreamPlayer2D.play()
