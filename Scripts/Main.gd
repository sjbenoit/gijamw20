extends Node

export (PackedScene) var Ball
export var ball_start_pos = Vector2()
export var max_score = 3

var left_score
var right_score
var the_balls = []
var camera_start_pos
var screen_shakes_remaining = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	print("Run './PinPong.x86_64' to start the game.")
	camera_start_pos = $Camera2D.position
	randomize()
	# Hook up targets to trigger spawners
	$TopTarget.connect("target_entered", self, "_on_top_target_entered")
	$BottomTarget.connect("target_entered", self, "_on_bottom_target_entered")
	# Hook up spawners to actually spawn balls
	$TopBallSpawner.connect("please_spawn_ball", self, "spawn_ball")
	$BottomBallSpawner.connect("please_spawn_ball", self, "spawn_ball")
	start_game()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	# Screenshake!
	if screen_shakes_remaining > 0:
		screen_shakes_remaining -= 1
		var offset = Vector2()
		offset.x = (randi()%25+25) * (randi()%2-1)
		offset.y = (randi()%25+25) * (randi()%2-1)
		$Camera2D.position = camera_start_pos + offset
	else:
		$Camera2D.position = camera_start_pos
	# Restart button
	if Input.is_action_just_pressed("restart_button"):
		start_game()
	# Exit button
	if Input.is_action_just_pressed("exit_button"):
		get_tree().quit()
	# Game over screen management
	if left_score >= max_score or right_score >= max_score:
		$CanvasLayer/GameOverTimerText.visible = true
		$CanvasLayer/GameOverTimerText.text = str(stepify($GameOverScreenTimer.time_left, 1))
	else:
		$CanvasLayer/GameOverTimerText.visible = false

func start_game():
	cleanup_ball()
	left_score = 0
	right_score = 0
	screen_shakes_remaining = 0
	$GameOverScreenTimer.stop()
	$GoalFanfarePlayer.stop()
	spawn_ball(ball_start_pos)
	update_scores()

func spawn_ball(pos):
	if left_score >= max_score or right_score >= max_score:
		# Don't spawn balls when the game should be over
		return
	var new_ball = Ball.instance()
	call_deferred("add_child", new_ball)
	new_ball.position = pos
	new_ball.connect("destroyed", self, "score_ball")
	the_balls.append(new_ball)

func cleanup_ball():
	$TopBallSpawner.cleanup()
	$BottomBallSpawner.cleanup()
	while not the_balls.empty():
		var old_ball = the_balls.pop_back()
		old_ball.disconnect("destroyed", self, "score_ball")
		old_ball.queue_free()

func score_ball(ball):
	if ball.position.x < 1:
		right_score += 1
		$LeftParticles.emitting = true
	else:
		left_score += 1
		$RightParticles.emitting = true
	$GoalFanfarePlayer.play()
	update_scores()
	# Trigger screenshake
	screen_shakes_remaining = 20
	# Clean up the ball that scored
	the_balls.remove(the_balls.find(ball))
	ball.disconnect("destroyed", self, "score_ball")
	ball.queue_free()
	if len(the_balls) < 1:
		spawn_ball(ball_start_pos)

func update_scores():
	$CanvasLayer/LeftScore.text = str(left_score) + "/" + str(max_score)
	$CanvasLayer/RightScore.text = str(right_score) + "/" + str(max_score)
	if left_score >= max_score:
		$CanvasLayer/VictoryLabel.text = "Left player wins!"
	elif right_score >= max_score:
		$CanvasLayer/VictoryLabel.text = "Right player wins!"
	else:
		$CanvasLayer/VictoryLabel.visible = false
		return
	# If we get here, somebody won the game (which you just lost)
	$CanvasLayer/VictoryLabel.visible = true
	$GameOverScreenTimer.start()
	cleanup_ball()

func _on_top_target_entered(object):
	$TopBallSpawner.start_ball_spawn()

func _on_bottom_target_entered(object):
	$BottomBallSpawner.start_ball_spawn()
