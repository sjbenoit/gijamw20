extends RigidBody2D

export var flip_angle = 30
export var flip_duration = 0.05
export var unflip_duration = 0.1

var flipping = false
var unflipping = false
var flip_time = 0
var flip_rate
var unflip_rate
var initial_rotation
var flip_button
var flip_direction

# Called when the node enters the scene tree for the first time.
func _ready():
	var parent = get_parent()
	flip_button = parent.flip_button
	flip_direction = parent.flip_direction

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	if Input.is_action_pressed(flip_button):
		#if not flipping and not unflipping:
		flipping = true
		#unflipping = false
	else:
		#unflipping = true
		flipping = false

func _integrate_forces(_state):
	if flipping:
		applied_torque = 1000000 * flip_direction
	else:#if unflipping:
		applied_torque = -1000000 * flip_direction

#func neverDo():
#	if flipping:
#		flip_time += delta
#		if flip_time > flip_duration:
#			flipping = false
#			unflipping = true
#			flip_time = 0
#		rotation_degrees += flip_rate * delta
#	elif unflipping:
#		flip_time += delta
#		if flip_time > unflip_duration:
#			unflipping = false
#			flip_time = 0
#		rotation_degrees -= unflip_rate * delta
#	else:
#		rotation_degrees = initial_rotation
