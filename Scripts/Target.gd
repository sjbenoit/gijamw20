extends Node2D

signal target_entered(object)
signal target_exited(object)

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func emit_target_entered(object):
	emit_signal("target_entered", object)

func emit_target_exited(object):
	emit_signal("target_exited", object)
