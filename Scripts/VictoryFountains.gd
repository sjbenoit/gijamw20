extends Node2D



# Called when the node enters the scene tree for the first time.
func _ready():
	stop_all()

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func stop_all():
	$Left/BlueParticles.emitting = false
	$Left/RedParticles.emitting = false
	$Right/BlueParticles.emitting = false
	$Right/RedParticles.emitting = false

func start_blue():
	$Left/BlueParticles.emitting = true
	$Right/BlueParticles.emitting = true

func start_red():
	$Left/RedParticles.emitting = true
	$Right/RedParticles.emitting = true
